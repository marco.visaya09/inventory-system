<?php

use App\Http\Controllers\ItemController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\VerificationCodesController;
use App\Http\Controllers\TransactionController;
use App\Exports\SalesExport ;
use Maatwebsite\Excel\Facades\Excel;

Route::prefix('user')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    
    Route::middleware('auth:sanctum')->group(function () {

        Route::get('/validate-token', function (Request $request) {return $request->user();});
        Route::get('/validate-email', function (Request $request) {return $request->user()->verified_at ? "true" : "false";});
        Route::get('/getUserData', [AuthController::class, 'getUserData']);

        Route::post('/update', [AuthController::class, 'update']);
        Route::post('/send-verification', [VerificationCodesController::class, 'sendVerificationCode']);
        Route::post('/check-verification', [VerificationCodesController::class, 'checkVerificationCode']);
        Route::post('/item/{id}', [ItemController::class, 'update']);
        Route::post('/transact', [TransactionController::class, 'transact']);

        Route::resource('/item', ItemController::class)->except(['create', 'edit']);
    });
});

Route::prefix('admin')->group(function () {
    Route::post('login', [AdminController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
      
        Route::get('/validate-token', function (Request $request) {return $request->user()->role; });
        Route::get('/getAdminData', [AdminController::class, 'getAdminData']);
        Route::get('/getAdmins', [AdminController::class, 'getAdmins']);
        Route::get('/fetchSystemSummary', [AdminController::class, 'fetchSystemSummary']);

        Route::post('register', [AdminController::class, 'register']);
        Route::post('/update', [AdminController::class, 'update']);
        Route::post('/fetchSales', [TransactionController::class, 'fetchSales']);
        Route::post('/excelExport', function(){return Excel::download(new SalesExport , 'sales.xlsx');});
        Route::post('/destroyAdmin', [AdminController::class, 'destroyAdmin']);
     
    });
});

Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');

Route::post('/fetchTransaction', [TransactionController::class, 'fetchTransaction'])->middleware('auth:sanctum');
Route::post('/finishTransaction', [TransactionController::class, 'finishTransaction'])->middleware('auth:sanctum');
Route::post('/updateTransactStatus', [TransactionController::class, 'updateTransactStatus'])->middleware('auth:sanctum');
