<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finished_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('finished_transaction_id');
            $table->unsignedBigInteger('product_id');
            $table->integer('quantity');

            $table->foreign('finished_transaction_id')->references('id')->on('finished_transactions')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finished_orders');
    }
};
