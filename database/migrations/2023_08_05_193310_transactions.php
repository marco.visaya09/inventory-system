<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('shipping_fee');
            $table->unsignedBigInteger('transaction_status');
            $table->string('payment_method');
            $table->string('comment');
            $table->string('canceled_comment')->nullable();
            $table->string('to_return_comment')->nullable();
            $table->string('proof_of_payment')->nullable();

            $table->timestamp('pending_at')->nullable();
            $table->timestamp('processing_at')->nullable();
            $table->timestamp('delivering_at')->nullable();
            $table->timestamp('completed_at')->nullable();

            $table->timestamp('to_return_requested_at')->nullable();
            $table->timestamp('to_return_accepted_at')->nullable();
            $table->timestamp('to_return_process_at')->nullable();
            

            
            $table->timestamp('return_completed_at')->nullable();
            // $table->timestamp('to_return_rejected_at')->nullable();
            // $table->timestamp('rejected_at')->nullable();
            // $table->timestamp('canceled_at')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('transaction_status')->references('id')->on('transaction_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
