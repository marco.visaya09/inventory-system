<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('added_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->string('name');
            $table->string('description');
            $table->double('price');
            $table->string('category');
            $table->tinyInteger('quantity');

            $table->string('img_url');
            $table->boolean('available');
            
            $table->foreign('added_by')
            ->references('id')
            ->on('admins')
            ->onDelete('cascade');

            $table->foreign('updated_by')
            ->references('id')
            ->on('admins')
            ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
};
