<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('roles')->insert([
            ['title' => 'SUPER ADMIN'],
            ['title' => 'ADMIN'],
        ]);

        DB::table('transaction_statuses')->insert([
            ['title' => 'PENDING'],
            ['title' => 'PROCESSING'],
            ['title' => 'DELIVERING'],
            ['title' => 'COMPLETED'],
            ['title' => 'TO RETURN REQUESTED'],
            ['title' => 'TO RETURN ACCEPTED'],
            ['title' => 'RETURN PROCESSING'],
            ['title' => 'RETURN COMPLETED'],
            ['title' => 'TO RETURN REJECTED'],
            ['title' => 'REJECTED'],
            ['title' => 'CANCELED'],
        ]);
        
        DB::table('users')->insert([
            'name' => Crypt::encryptString("DUMMY USER"),
            'phone_number' => Crypt::encryptString('09395206141'),
            'address' => Crypt::encryptString('BATASAN A. MALAPIT QUEZON CITY'),
            'email' => 'user@gmail.com',
            'password' => bcrypt('123456'),
            'verified_at' => Carbon::now()
        ]);

        DB::table('admins')->insert([
            'name' => Crypt::encryptString("SUPER ADMIN"),
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 1,
        ]);

        DB::table('items')->insert([
            'added_by' => 1,
            'name' => 'Burger',
            'description' => 'A burger',
            'img_url' => '',
            'price' => 12,
            'category' => 'SHORT ORDER',
            'available' => 1,
            'quantity' => 12
        ]);

        DB::table('items')->insert([
            'added_by' => 1,
            'name' => 'Fries',
            'description' => 'A fries',
            'img_url' => '',
            'category' => 'SHORT ORDER',
            'price' => 12,
            'available' => 1,
            'quantity' => 15
   
        ]);


     
    }
}
