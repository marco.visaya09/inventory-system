<!DOCTYPE html>
<html>
<head>
    <title>Email Verification</title>
    <style>
        body {
            background-color: #f7f7f7;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 40px 20px;
            text-align: center;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }

        h1 {
            font-size: 24px;
            color: #333333;
            margin-bottom: 20px;
        }

        p {
            font-size: 16px;
            color: #555555;
            margin-bottom: 20px;
        }

        .verification-code {
            font-size: 20px;
            color: #007bff;
            margin-bottom: 40px;
        }

        .message {
            font-size: 16px;
            color: #777777;
            margin-bottom: 40px;
        }

        .footer {
            font-size: 14px;
            color: #999999;
            margin-top: 40px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Email Verification</h1>
        <p>Hi {{ $data['user'] ?? 'Juan DelaCruz' }},</p>
        <p>Please use the following code to verify your email address:</p>
        <p class="verification-code">{{ $data['code'] ?? '****' }}</p>
        <p class="message">Thank you for joining our platform! We're excited to have you on board. Verifying your email helps us ensure the security and authenticity of our users.</p>
        <p class="footer">If you have any questions, feel free to contact our support team.</p>
    </div>
</body>
</html>
