import { createRouter, createWebHistory } from "vue-router";

// import ExampleComponent from "../components/ExampleComponent.vue";

import Landing from "../components/landing/Index.vue";

import Login from "../components/user/auth/LoginPage.vue";
import Register from "../components/user/auth/RegisterPage.vue";
import ManageAccountUser from "../components/user/auth/ManageAccount.vue";
import Verify from "../components/user/auth/VerifyUser.vue";
import Dashboard from "../components/user/dashboard/Index.vue";
import PlaceOrder from "../components/user/orders/PlaceOrder.vue";
import ViewOrdersUser from "../components/user/orders/ViewOrdersUser.vue";

import LoginAdmin from "../components/admin/auth/LoginPage.vue";
import ManageAccountAdmin from "../components/admin/auth/ManageAccount.vue";
import AdministratorManager from "../components/admin/AdministratorManager.vue";
import Read from "../components/admin/product/Read.vue";
import DashboardAdmin from "../components/admin/Dashboard.vue";
import Create from "../components/admin/product/Create.vue";
import Update from "../components/admin/product/Update.vue";
import ViewAllOrders from "../components/admin/orders/ViewAllOrders.vue";

import NotFound from "../components/NotFoundError.vue";

//Import other vue components here

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/landing",
            name: "Landing",
            component: Landing,
            meta: { guest: true },
        },
        {
            path: "/login",
            name: "Login",
            component: Login,
            meta: { guest: true },
        },
        {
            path: "/register",
            name: "Register",
            component: Register,
            meta: { guest: true },
        },
        {
            path: "/login-admin",
            name: "login-admin",
            component: LoginAdmin,
            meta: { guest: true },
        },

        
        {
            path: "/verify",
            name: "Verify",
            component: Verify,
            meta: { user: true },
        },
        {
            path: "/account-user",
            name: "manage-account-user",
            component:  ManageAccountUser,
            meta: { user: true },
        },
        {
            path: "/",
            name: "dashboard",
            component: Dashboard,
            meta: { user: true },
        },
        {
            path: "/placeorder",
            name: "placeorder",
            component: PlaceOrder,
            meta: { user: true },
        },
        {
            path: "/vieworders",
            name: "vieworders",
            component: ViewOrdersUser,
            meta: { user: true },
        },


        {
            path: "/admin",
            name: "dashboard-admin",
            component: DashboardAdmin,
            meta: { admin: true },
        },
        {
            path: "/account-admin",
            name: "manage-account-admin",
            component: ManageAccountAdmin,
            meta: { admin: true },
        },
        {
            path: "/administrator",
            name: "administrator",
            component: AdministratorManager,
            meta: { admin: true },
        },
        {
            path: "/product/read",
            name: "read",
            component: Read,
            meta: { admin: true },
        },
        {
            path: "/product/create",
            name: "create",
            component: Create,
            meta: { admin: true },
        },
        {
            path: "/viewtransactions",
            name: "viewallorders",
            component: ViewAllOrders,
            meta: { admin: true },
        },      
        {
            path: "/product/update/:id",
            name: "update",
            component: Update,
            meta: { admin: true },
        },
       

        {
            path: "/:catchAll(.*)",
            component: NotFound,
        },
    ],
});

function isUser() {
    return localStorage.getItem("user_token") ||
        sessionStorage.getItem("user_token")
        ? true
        : false;
}

function isAdmin() {
    return localStorage.getItem("admin_token") ||
        sessionStorage.getItem("admin_token")
        ? true
        : false;
}

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.user)) {
        if (!isUser()) {
            next({
                path: "/landing",
            });
        } else if (isAdmin()) {
            next({
                path: "/admin",
            });
        } else {
            next();
        }
    } else if (to.matched.some((record) => record.meta.admin)) {
        if (!isAdmin()) {
            next({
                path: "/landing",
            });
        } else if (isUser()) {
            next({
                path: "/",
            });
        } else {
            next();
        }
    } else if (to.matched.some((record) => record.meta.guest)) {
        if (isUser()) {
            next({
                path: "/",
            });
        } else if (isAdmin()) {
            next({
                path: "/admin",
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;
