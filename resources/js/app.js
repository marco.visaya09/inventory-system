import './bootstrap';
import { createApp } from 'vue';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from '../js/router'
import { RouterView } from 'vue-router';
import userValidator from './components/helpers/userValidator';
import "../sass/app.scss";
import { ref } from 'vue';
import '../sass/app.scss'
import MenuBarComponent from './components/user/MenuBarComponent.vue';
import AdminMenuBarComponent from "./components/admin/AdminMenuBarComponent.vue";

const {validateToken, validateAdminToken} = userValidator
const app = createApp({});

app.component('router-view', RouterView);
app.component('MenuBar', MenuBarComponent);
app.component('AdminMenuBar', AdminMenuBarComponent);
app.use(router);

app.use(ElementPlus);

app.mount('#app');

window.addEventListener('popstate', function() {
  location.reload();
});
