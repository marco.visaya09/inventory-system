import { ElNotification } from 'element-plus';

export const notify = (title, message, typeNumber) => {
    const types = {
        1: 'success',
        2: 'warning',
        3: 'info',
        4: 'error'
      };

      const type = types[typeNumber]

  ElNotification({
    title,
    message,
    type,
  });
}
