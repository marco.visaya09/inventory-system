import axios from "axios";
import { ref } from "vue";
import formHelper from "./form";
import { notify } from "../common/notification";
import { Alert } from "bootstrap";
const { formRef, form, resetForm } = formHelper;

const tokenName = "admin_token";
const loading = ref(false);
const payload = ref({});

const currentToken = () => {
    return localStorage.getItem(tokenName) ?? sessionStorage.getItem(tokenName)
}

const getUserData = async (data) => {
    const getAdminData_URL = '/api/admin/getAdminData'
    try {
        loading.value = true;

        const headers = {Authorization: `Bearer ${currentToken()}`};
        const response = await axios({
            method: 'GET',
            url: getAdminData_URL,
            headers: headers,
        });

        data.value = response.data;


        console.log(response);
    } catch (error) {
        console.log(error);
    } finally {
        loading.value = false;
    }
};

const performRequest = async (URL, payloadProperties, requestType, router) => {
    try {
        var validate = await formRef.value.validate();
        loading.value = true;
        payloadProperties.forEach((prop) => {
            payload.value[prop] = form.value[prop];
        });
        let response = await axios.post(URL, payload.value, {
            headers: {
                "X-Request-Type": requestType,
                "Authorization" : requestType == "register" ? `Bearer ${currentToken()}` : null,
            },
        });

        if (requestType === "login") {
            if (form.value.remember) {
                localStorage.setItem("admin_token", response.data.token);
            } else {
                sessionStorage.setItem("admin_token", response.data.token);
            }
            resetForm();
            router.push("/admin");
        }
        notify("Success", "Login Successfully", 1);
    } catch (error) {
        if (!validate) {
            notify("Error", "The input fields cannot be validated.", 4);
            form.value.password = "";
            form.value.password_confirm = "";
        } else {
            notify(error.code, error.message, 4);
        }
    } finally {
        loading.value = false;
    }
};

const logout = (URL, router) => {
    
    try{
        loading.value = true;
        const headers = {Authorization: `Bearer ${currentToken()}`};
        axios({
            method: 'POST',
            url: URL,
            headers: headers,
        });

        sessionStorage.clear();
        localStorage.removeItem("admin_token");
        notify("Logout", "You have been logout successfully.", 1);
        router.push("/login-admin");

    }catch(error){
        notify("Logout error", "Something went wrong", 4);
        console.log(error);
    }finally{
        loading.value = false;
    }

  };
  





export default { loading, performRequest, logout, getUserData };
