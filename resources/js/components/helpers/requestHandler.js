import axios from 'axios';
import { ref } from 'vue';
import formHelper from "./form"
import { notify } from '../common/notification';

const { formRef, form, resetForm } = formHelper;
const loading = ref(false);
const response = ref();

const makeRequest = async (url, method, payload, picture, available, dataRef) => {
  window.axios.defaults.headers.common['Authorization'] =
    `Bearer ${localStorage.getItem('admin_token') ?? sessionStorage.getItem('admin_token')}`;

  try {
    loading.value = true;
    const requestData = {};

    if (payload) {
      payload.forEach(prop => {
        requestData['available'] = available;

        if (prop !== 'picture') {
          requestData[prop] = form.value[prop];
        } else {
          requestData['picture'] = picture;
        }
      });
    }

    const headers = {
      'Authorization': `Bearer ${localStorage.getItem('admin_token') ?? sessionStorage.getItem('admin_token')}`,
    };

    if (picture === null) {
      headers['Content-Type'] = 'application/json';
    } else {
      headers['Content-Type'] = 'multipart/form-data';
    }

    const response = await axios({
      method: method,
      url: url,
      data: requestData,
      headers: headers,
    });

    dataRef.value = response.data;
  } catch (error) {
    // Handle error
  } finally {
    loading.value = false;
  }
};

export default { loading, makeRequest };
