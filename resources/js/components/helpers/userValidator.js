import axios from "axios";

const adminTokenName = "admin_token";
const userTokenName = "user_token";

let currentToken = (name) => {
    return localStorage.getItem(name) ?? sessionStorage.getItem(name);
};

const validateToken = (router) => {
    let token = currentToken(userTokenName);
    if (token) {
        return axios
            .get("/api/user/validate-token", {
                headers: { Authorization: `Bearer ${token}` },
            })
            .then((response) => response.data)
            .catch((error) => {
                if (error.response && error.response.status === 401) {
                    localStorage.removeItem(userTokenName);
                    sessionStorage.removeItem(userTokenName);

                    router.push("/");
                }
            });
    }
};

const validateAdminToken = (router) => {
    let token = currentToken(adminTokenName);

    if (token) {
        return axios
            .get("/api/admin/validate-token", {
                headers: { Authorization: `Bearer ${token}` },
            })
            .then((response) => response.data)
            .catch((error) => {
                if (error.response && error.response.status === 401) {
                    localStorage.removeItem(adminTokenName);
                    sessionStorage.removeItem(adminTokenName);

                    router.push("/");
                }
            });
    }
};

const validateEmail = async (router, route) => {
    try {
        const headers = {
            Authorization: `Bearer ${currentToken(userTokenName)}`,
        };
        const res = await axios({
            method: "GET",
            url: "/api/user/validate-email",
            headers: headers,
        });

        const isVerified = res.data;
        if (!isVerified) {
            router.push({ name: "Verify" });
        } else {
            if (route.name === "Verify") {
                router.push("/");
            }
        }
    } catch (error) {
        console.log("Validate email error: " + error);
    }
};

export default { validateToken, validateAdminToken, validateEmail };
