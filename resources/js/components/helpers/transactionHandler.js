import axios from "axios";
import { ref } from "vue";
import formHelper from "./form";
import { notify } from "../common/notification";

const { formRef, form, resetForm } = formHelper;
const transactionLoading = ref(false);
const response = ref();

const fetchOrders = async (dataRef, access, isFinished) => {
    try {
        transactionLoading.value = true;
        
        let token = localStorage.getItem("user_token") ?? sessionStorage.getItem("user_token") ?? localStorage.getItem("admin_token") ?? sessionStorage.getItem("admin_token");
        const headers = {
            Authorization: `Bearer ${token}`,
        };

        const fetchData = {
          transaction_user : access == "user" ? "user" : "admin",  
          transaction_type: isFinished == "finished" ? "finished" : "ongoing",

        };
        let res = await axios({
            method: "POST",
            url:  "/api/fetchTransaction",
            data: fetchData,
            headers: headers,
        });
        dataRef.value = res.data;
    } catch (error) {
        dataRef.value = [];
        console.log(error);
    } finally {
        transactionLoading.value = false;
    }
};

const finishOrder = async(finishOrderData) => {
  const finishOrderUrl = "/api/finishTransaction"

  try {
    transactionLoading.value = true;
    const headers = {
        Authorization: `Bearer ${localStorage.getItem("admin_token") ??
            sessionStorage.getItem("admin_token")
            }`,
    };
    await axios({
        method: "POST",
        url: finishOrderUrl,
        data: finishOrderData,
        headers: headers,
    });
} catch (error) {
    console.log(error);
} finally {
    transactionLoading.value = false;

}
}

const ongoingStatusOptions = ref  ([
    { label: 'Sort by Status', value: '' },
    { label: 'Pending', value: '1' },
    { label: 'Processing', value: '2' },
    { label: 'Delivering', value: '3' },
    { label: 'Completed', value: '4' },
    { label: 'To Return Requested', value: '5' },
    { label: 'To Return Accepted', value: '6' },
    { label: 'Return Processing', value: '7' },
    { label: 'Return Completed', value: '8' }
]);

const finishedStatusOptions = ref  ([
    { label: 'Sort by Status', value: '' },
    { label: 'Successful', value: '4' },
    { label: 'Return Completed', value: '8' },
    { label: 'To Return Rejected', value: '9' },
    { label: 'Rejected', value: '10' },
    { label: 'Canceled', value: '11' },
]);

const sortingOptions = ref  ([
    { label: 'Sort by Date', value: '' },
    { label: 'Created At', value: '1' },
    { label: 'Updated at', value: '2' },
]);

export default { transactionLoading, fetchOrders, finishOrder, ongoingStatusOptions, finishedStatusOptions, sortingOptions };
