import axios from "axios";
import { ref } from "vue";
import formHelper from "./form";
import { notify } from "../common/notification";

const { form } = formHelper;
const loading = ref(false);

const fetchItem = async (url, dataRef) => {
    try {
        loading.value = true;
        const response = await axios({
            method: "GET",
            url: url,
            headers: {
                Authorization: `Bearer ${
                    localStorage.getItem("admin_token") ??
                    sessionStorage.getItem("admin_token") ??
                    localStorage.getItem("user_token") ?? 
                    sessionStorage.getItem("user_token")
                }`,
            },
        });
        dataRef.value = response.data;
    } catch (error) {
        console.log(error);
    } finally {
        loading.value = false;
    }
};

const processItem = async (
    url,
    method,
    payload,
    picture,
    available,
    category,
    router
) => {
    try {
        loading.value = true;

        const headers = {
            Authorization: `Bearer ${
                localStorage.getItem("admin_token") ??
                sessionStorage.getItem("admin_token")
            }`,
            "Content-Type":
                picture === null ? "application/json" : "multipart/form-data",
        };

        const requestData = await buildRequestData(payload, picture);
        requestData.append("available", available);
        requestData.append("category", category);
        await axios({
            method: method,
            url: url,
            data: requestData,
            headers: headers,
            _method: "patch",
        });
        notify("Success!", "The item was processed successfully", 1);
        router.push("/product/read");
    } catch (error) {
        notify("Oops!", "Something went wrong, please try again.", 4);
        console.log(error);
    } finally {
        loading.value = false;
    }
};

const buildRequestData = (payload, picture) => {
    const formData = new FormData();
    if (payload) {
        payload.forEach((prop) => {
            if (prop !== "picture" && prop !== "available") {
                formData.append(prop, form.value[prop]);
            }
        });
    }
    if (picture !== null) {
        formData.append("picture", picture);
    }

    return formData;
};

const destroyItem = async (url) => {
    try {
        loading.value = true;

        const headers = {
            Authorization: `Bearer ${
                localStorage.getItem("admin_token") ??
                sessionStorage.getItem("admin_token")
            }`,
        };
        await axios({
            method: 'DELETE',
            url: url,
            headers: headers,
        });
        notify("Success!", "The item was deleted successfully", 1);
    } catch (error) {
        notify("Oops!", "Something went wrong, please try again.", 4);
        console.log(error);
    } finally {
        loading.value = false;
    }
}

export default { loading, fetchItem, processItem, destroyItem };
