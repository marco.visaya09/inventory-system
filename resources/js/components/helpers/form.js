import {ref } from "vue";

const formSize = "large";
const formRef = ref(null);
const form = ref({});

const resetForm = () => {
    form.value = ({});
    formRef.value.resetFields();
  };

function validatePasswordConfirmation(rule, value, callback) {
if (value === '') {
    callback(new Error('Please confirm your password'));
} else if (value !== form.value.password) {
    callback(new Error('Passwords do not match'));
} else {
    callback();
}
}

const required = { required: true, message: "This field is required", trigger: "blur" };

const rules = {
  required: required,

  number:{
    required,
    type: "number",
    message: "Please input a number",
    trigger: "change",
  },
  email: {
    required,
    type: "email",
    message: "Please enter a valid email",
    trigger: "change",
  },
  password: {
    required,
    min: 6,
    message: "Please make a stronger password",
    trigger: "change",
  },
  password_confirmation: {
    required,
    validator: validatePasswordConfirmation,
    trigger: "change",
  },
};

export default { formSize, formRef, form, resetForm, rules };
