import axios from "axios";
import { ref } from "vue";
import formHelper from "./form";
import { notify } from "../common/notification";
const { formRef, form, resetForm } = formHelper;

const tokenName = "user_token";
const loading = ref(false);
const payload = ref({});

const currentToken =  () => {
    return localStorage.getItem(tokenName) ?? sessionStorage.getItem(tokenName)
}

const getUserData = async (data) => {
    
    const getUserData_URL = '/api/user/getUserData'
    try {
        loading.value = true;

        const headers = {Authorization: `Bearer ${currentToken()}`};
        const response = await axios({
            method: 'GET',
            url: getUserData_URL,
            headers: headers,
        });

        data.value = response.data;
        console.log(response);
    } catch (error) {
        notify("Error", error, 4);
        console.log(error);
    } finally {
        loading.value = false;
    }


}

const performRequest = async (URL, payloadProperties, requestType, router) => {
    try {
        var validate = await formRef.value.validate();
        loading.value = true;
        payloadProperties.forEach((prop) => {
            payload.value[prop] = form.value[prop];
        });
        let response = await axios.post(URL, payload.value, {
            headers: {
                "X-Request-Type": requestType,
            },
        });

        notify("Success", " ", 1);
        if (requestType === "login") {
            if (form.value.remember) {
                localStorage.setItem("user_token", response.data.token);
            } else {
                sessionStorage.setItem("user_token", response.data.token);
            }
            router.push("/");
        } else {
            resetForm();
            router.push('/login');
        }
    } catch (error) {
        console.log(error);

        if (!validate) {
            notify("Error", "The input fields cannot be validated.", 4);
            form.value.password = "";
            form.value.password_confirm = "";
        } else {
            notify(error.code, error?.response?.data?.message ?? "Something went wrong, please contact the administrator.", 4);
        }
    } finally {
        loading.value = false;
    }
};

const logout = (URL, router) => {
    try{
        loading.value = true;
        const headers = {Authorization: `Bearer ${currentToken()}`};
        axios({
            method: 'POST',
            url: URL,
            headers: headers,
        });

        sessionStorage.clear();
        localStorage.removeItem("user_token");
        notify("Logout", "You have been logout successfully.", 1);
        router.push("/login");

    }catch(error){
        notify("Logout error", "Something went wrong", 4);
        console.log(error);
    }finally{
        loading.value = false;
    }

  };



export default { loading, performRequest, logout, getUserData };
