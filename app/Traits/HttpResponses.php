<?php

namespace App\Traits;
trait HttpResponses{
    protected function success($data, $message = null, $code = 200)
    {
        $responseData = [
            'status' => 'Request was successful',
            'message' => $message,
        ];
        if (is_array($data)) {
            $responseData = array_merge($responseData, $data);
        }
        return response()->json($responseData, $code);
    }


    protected function error($data, $message = null, $code)
    {
        $responseData = [
            'status' => 'Error has occurred...',
            'message' => $message,
        ];
        if (is_array($data)) {
            $responseData = array_merge($responseData, $data);
        }
        return response()->json($responseData, $code);
    }


}
