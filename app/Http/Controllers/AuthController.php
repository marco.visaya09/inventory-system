<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Traits\HttpResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationEmail;

class AuthController extends Controller
{
    use HttpResponses;

    public function getUserData(Request $request){
        $user = auth()->user();
        if($user->verified_at){
            $user = User::where("id", auth()->user()->id)->first();
            $user->name = Crypt::decryptString($user->name);
            $user->address = Crypt::decryptString($user->address);
            $user->phone_number =  Crypt::decryptString($user->phone_number);
            return $user;
        }else{
            $user->name = Crypt::decryptString($user->name);

            $phoneNumber = Crypt::decryptString($user->phone_number);
            $formattedNumber = substr($phoneNumber, 0, 2) . "******" . substr($phoneNumber, -2);
            $user->phone_number = $formattedNumber;

            $formattedEmail = substr($user->email, 0, 4) . "******";
            $user->email = $formattedEmail;

            return $user;
        }
    }

    public function login(LoginUserRequest $request){
            try{
                $request->validated($request->all());

                if (!Auth::attempt($request->only('email', 'password'))) {
                    return $this->error('', 'Credentials do not match', 401);
                }

                $user = User::where('email', $request->email)->first();

                if (!$user) {
                    return $this->error('User not found', 'User not found', 401);
                }

                return $this->success([
                    'user' => $user,
                    'token' => $user->createToken('API Token of ' . $user->name)->plainTextToken
                ]);
            }catch(\Exception $e){
                return $this->error('Error', $e->getMessage(), 500);
            }
    }

    public function register(StoreUserRequest $request){
        try {
            $request->validated($request->all());

            $user = User::create([
                'name' => Crypt::encryptString($request->name),
                'phone_number' => Crypt::encryptString($request->phone_number),
                'address' =>  Crypt::encryptString($request->address),                
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return $this->success([
                'user' => $user,
            ]);
        }catch(\Exception $e){
            return $this->error('User creation failed', $e->getMessage(), 500);
        }
    }

    public function update(Request $request) {
        $user = User::find(auth()->user()->id);
    
        if (!$user) {
            return response()->json(['message' => 'user not found'], 404);
        }
    
        // Define the validation rules
        $rules = [
            'name' => 'sometimes|string|max:255',
            'address' => 'sometimes|string',
            'phone_number' => 'sometimes|string|max:11',
            'email' => 'sometimes|email|unique:users,email,' . $user->id,
            'new_password' => 'sometimes|string|min:6',
        ];
    
        $validatedData = $request->validate($rules);
    
        $user->fill($validatedData);
    
        if ($request->has('name')) {
            $user->name = Crypt::encryptString($request->name);
        }

        if ($request->has('phone_number')){
            $user->phone_number = Crypt::encryptString($request->phone_number);
        }

        if ($request->has('address')) {
            $user->address = Crypt::encryptString($request->address);
        }
    
        if ($request->has('new_password')) {
            $user->password = Hash::make($request->new_password);
        }
    
        $user->save();
    
        return response()->json(['message' => 'user updated successfully']);
    }


    public function logout(){
        // Auth::user()->currentAccessToken()->delete();
        $user = auth()->user();
        $user->tokens()->delete();
        return $this->success([
            'message' => 'You have successfully logout and your token has been deleted.'
        ]);
    }


   
}
