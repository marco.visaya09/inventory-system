<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\Admin;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\StoreAdminRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\AuthAdmin;
use Illuminate\Http\Request;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\FinishedTransaction;
use App\Models\User;
use App\Models\Transaction;



class AdminController extends Controller
{
    use HttpResponses;

    public function getAdminData(Request $request){
        $admin = Admin::where("id", auth()->user()->id)->with("roles")->first();
        $admin->name = Crypt::decryptString($admin->name);
        return $admin;
    }
    public function login(LoginUserRequest $request)
    {
        try {
            $request->validated($request->all());

            if (!Auth::guard('admin')->attempt($request->only('email', 'password'))) {
                return $this->error('', 'Credentials do not match', 401);
            }

            $admin = Admin::where('email', $request->email)->first();

            return $this->success([
                'admin' => $admin,
                'token' => $admin->createToken('API Token of ' . $admin->name)->plainTextToken
            ]);
        } catch (\Exception $e) {
            return $this->error('Error', $e->getMessage(), 500);
        }
    }
    public function register(StoreAdminRequest $request) 
    {
        if($this->isNotAuthorized()){
            return $this->isNotAuthorized();
        }

        $admin = auth()->user();

        try {
            DB::beginTransaction();
            $request->validated($request->all());
            $admin = Admin::create([
                'name' => Crypt::encryptString($request->name),
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role ?? 2,
                'created_by' => $admin->id
            ]);

            DB::commit();
            return $this->success([
                'admin' => $admin,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->error('Error', $e->getMessage(), 500);
        }
    }

    public function update(Request $request) {
        $admin = Admin::find(auth()->user()->id);
    
        if (!$admin) {
            return response()->json(['message' => 'Admin not found'], 404);
        }
    
        // Define the validation rules
        $rules = [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email|unique:admins,email,' . $admin->id,
            'new_password' => 'sometimes|string|min:6',
        ];
    
        $validatedData = $request->validate($rules);
    
        $admin->fill($validatedData);
    
        if ($request->has('name')) {
            $admin->name = Crypt::encryptString($request->name);
        }
    
        if ($request->has('new_password')) {
            $admin->password = Hash::make($request->new_password);
        }
    
        $admin->save();
    
        return response()->json(['message' => 'Admin updated successfully']);
    }
    


    public function getAdmins(){
        if($this->isNotAuthorized()){
            return $this->isNotAuthorized();
        }

        return Admin::where("role", "!=", 1)->get()->map(function ($admin) {
            $admin->name = Crypt::decryptString($admin->name);
            return $admin;
        });
    }
    
    public function destroyAdmin(Request $request){
        if($this->isNotAuthorized()){
            return $this->isNotAuthorized();
        }
        $adminId = $request->id;
        Admin::destroy($adminId);
    }

    public function fetchSystemSummary(){

        $revenue = 0;
        FinishedTransaction::with('user', 'orders.item', 'transactionStatus')
            ->where('transaction_status', 4)
            ->get()
            ->transform(function ($transaction) use (&$revenue) {
                foreach ($transaction->orders as $order) {
                    $order->item_sub_total = $order->quantity * $order->item->price;
                    $revenue += $order->item_sub_total; // Change to use the correct property
                }
            });
        $userCount = User::count();
        $adminCount = Admin::count();
        $transactionCount = Transaction::count();
        $finishedTransactionCount = FinishedTransaction::count();
    
        $summaryData = [
            [
                'details' => 'Users',
                'header' => $userCount,
            ],
            [
                'details' => 'Admins',
                'header' => $adminCount,
            ],
            [
                'details' => 'Ongoing Transactions',
                'header' => $transactionCount,
            ],
            [
                'details' => 'Finished Transactions',
                'header' => $finishedTransactionCount,
            ],
            [
                'details' => 'Total Revenue Made',
                'header' => $revenue,
            ],
            
        ];
    
        return response()->json($summaryData);
    }
    
    private function isNotAuthorized(){
        if(Auth::user()->role != 1){
            return $this->error('', 'You are not  authorized to make this request', 403);
        }
    }

    


}
