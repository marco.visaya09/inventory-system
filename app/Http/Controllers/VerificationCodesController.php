<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\VerificationCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationEmail;
use Carbon\Carbon;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\Crypt;

use Illuminate\Support\Facades\Hash;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\DB;

class VerificationCodesController extends Controller
{
    use HttpResponses;
  

    function sendVerificationCode(Request $request)
    {

        $method = $request->method;
        $user = $request->user();
        $userId = $user->id;

        $verificationCode = VerificationCodes::where('user_id', $userId)->first();

        if ($verificationCode) {
            $timestamp = Carbon::parse($verificationCode->time_stamp);
            $currentTime = Carbon::now();

            $minutesDifference = $timestamp->diffInMinutes($currentTime);

            if ($minutesDifference > 5) {
                if ($method === 'email') {
                    try {
                        DB::beginTransaction();
                        $this->sendEmailVerification($request);
                        DB::commit();
                        return $this->success(['message' => 'Your verification code was sent successfully.']);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return $this->error('', $e->getMessage(), 400);
                    }
                } else if ($method === 'sms') {
                    try {
                        DB::beginTransaction();
                        $this->sendSmsVerification($request);
                        DB::commit();
                        return $this->success(['message' => 'Your verification code was sent successfully.']);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        if ($e->getCode() == 21608) {
                            return $this->error('', 'We cannot send a verification code into unverified sim cards.', 403);
                        } else {
                            return $this->error('', $e->getMessage(), 400);
                        }
                    }
                }
            } else {
                // Prevent the verification attempt, as it hasn't been 5 minutes yet
                return $this->error('', 'You can only request a verification code once every 5 minutes.', 400);
            }
        } else {
            if ($method === 'email') {
                try {
                    DB::beginTransaction();
                    $this->sendEmailVerification($request);
                    DB::commit();
                    return $this->success(['message' => 'Your verification code was sent successfully.']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return $this->error('', $e->getMessage(), 400);
                }
            } else if ($method === 'sms') {
                try {
                    DB::beginTransaction();
                    $this->sendSmsVerification($request);
                    DB::commit();
                    return $this->success(['message' => 'Your verification code was sent successfully.']);
                } catch (\Exception $e) {
                    DB::rollBack();
                    if ($e->getCode() == 21608) {
                        return $this->error('', 'We cannot send a verification code into unverified sim cards.', 403);
                    } else {
                        return $this->error('', $e->getMessage(), 400);
                    }
                }
            }
        }
    }

    function sendEmailVerification($request)
    {
        $user = $request->user();
        $userId = $user->id;

        $code = $this->generateVerificationCode();

        VerificationCodes::updateOrCreate(
            ['user_id' => $userId],
            ['code' => bcrypt($code), 'time_stamp' => Carbon::now()]
        );

        $data = [
            'user' => Crypt::decryptString($user->name),
            'code' => $code,
        ];
        Mail::to($user->email)->send(new VerificationEmail($data));
    }

    function sendSmsVerification($request)
    {
        $user = $request->user();
        $userId = $user->id;
        $phoneNumber = preg_replace('/[^0-9]/', '', Crypt::decryptString($user->phone_number));
        $formattedPhoneNumber = (substr($phoneNumber, 0, 1) === '0') ? '+63' . substr($phoneNumber, 1) : '+' . $phoneNumber;

        $code = $this->generateVerificationCode();
        $message = config('app.name') . ' Verification code: ' . $code;

        VerificationCodes::updateOrCreate(
            ['user_id' => $userId],
            ['code' => bcrypt($code), 'time_stamp' => Carbon::now()]
        );


        $twilio = new Client(config('services.twilio.account_sid'), config('services.twilio.auth_token'));
        $message = $twilio->messages->create(
            $formattedPhoneNumber,
            // To phone number
            [
                'from' => config('services.twilio.from_phone_number'),
                // Your Twilio phone number
                'body' => $message
            ]
        );
    }

    function checkVerificationCode(Request $request)
    {
        $submittedCode = $request->verification_code;

        $user = $request->user();
        $userId = $user->id;

        $verificationCode = VerificationCodes::where('user_id', $userId)->first();
        $generatedCode = $verificationCode->code;


        if (Hash::check($submittedCode, $generatedCode)) {
            $user->update([
                'verified_at' => Carbon::now()
            ]);

        $verificationCode->delete();
        return $this->success(['message' => 'You successfully verified your account.']);
        
        } else {
            return $this->error('', 'Code did not match', 401);
        }
    }

    function generateVerificationCode()
    {
        $chars = "aBcDeFgHiJkMnOpQrStUvWxYz023456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $pass = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }
}