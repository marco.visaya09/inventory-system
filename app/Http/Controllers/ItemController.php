<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreItemRequest;
use App\Http\Resources\ItemResource;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\HttpResponses;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    use HttpResponses;

    public function index()
    {
        $user = Auth::user();
        if (!$user->role) {
            return Item::where('available', 1)->get();
        }else{
            return Item::all();
        }
       
    }

    public function store(StoreItemRequest $request)
    {
        // if ($this->isNotAuthorized()) {
        //     return $this->isNotAuthorized();
        // }
    
        DB::beginTransaction();
    
        try {
            $request->validated($request->all());
    
            $pictureName = '';
            if ($request->hasFile('picture')) {

                $validatePicture = Validator::make($request->all(), [
                    'picture' => 'required|file|max:1000', // Maximum size of 1000 KB (1 MB)
                ]);
                
                if ($validatePicture->fails()) {
                    return $this->error('', 'The image was too large, we only allowed 1000mb', 402);
                }

                $picture = $request->file('picture');
                $pictureName = time() . '.' . $picture->getClientOriginalExtension();
                $picture->move(public_path('pictures'), $pictureName);
            }
    
            $item = Item::create([
                'added_by' => Auth::user()->id,
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'category' => $request->category,
                'quantity' => $request->quantity,
                'img_url' => $pictureName,
                'available' => filter_var($request->available, FILTER_VALIDATE_BOOLEAN),
                
            ]);
    
            DB::commit();
    
            return new ItemResource($item);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function show(Item $item)
    {
        return new ItemResource($item);
    }

    public function update($id, Request $request)
    {
        if ($this->isNotAuthorized()) {
            return $this->isNotAuthorized();
        }
        DB::beginTransaction();
        try {
            $item = Item::findOrFail($id);
    
            $item->update([
                'updated_by' => Auth::user()->id,
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'quantity' => $request->quantity,
                'available' => filter_var($request->available, FILTER_VALIDATE_BOOLEAN)
            ]);
    
            if ($request->hasFile('picture')) {

                $validatePicture = Validator::make($request->all(), [
                    'picture' => 'required|file|max:1000', // Maximum size of 1000 KB (1 MB)
                ]);
                
                if ($validatePicture->fails()) {
                    return $this->error('', 'The image was too large, we only allowed 1000mb', 402);
                }

                $picture = $request->file('picture');
                $pictureName = time() . '.' . $picture->getClientOriginalExtension();
    
                if (File::exists(public_path('pictures/' . $item->img_url))) {
                    File::delete(public_path('pictures/' . $item->img_url));
                }
    
                $picture->move(public_path('pictures'), $pictureName);
    
                $item->update([
                    'img_url' => $pictureName,
                ]);
            }
            DB::commit();
    
            return new ItemResource($item);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function destroy(Item $item)
    {
        if($this->isNotAuthorized()){
            return $this->isNotAuthorized();
        }

        if (File::exists(public_path('pictures/' . $item->img_url))) {
            File::delete(public_path('pictures/' . $item->img_url));
        }

        $item->delete();
        return response(null,204);
    }

    private function isNotAuthorized(){
        if(!Auth::user()->role){
            return $this->error('', 'You are not  authorized to make this request', 403);
        }
    }
}
