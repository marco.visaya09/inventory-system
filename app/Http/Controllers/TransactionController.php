<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\TransactionStatus;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\FinishedTransaction;
use App\Models\FinishedOrder;

use App\Models\Order;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Events\MessageNotification;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function transact(Request $request)
    {
       
        try {
          

            if ($request->hasFile('picture')) {

                $validatePicture = Validator::make($request->all(), [
                    'picture' => 'required|file|max:1000', // Maximum size of 1000 KB (1 MB)
                ]);
                
                if ($validatePicture->fails()) {
                    return $this->error('', 'The image was too large, we only allowed 1000mb', 402);
                }

                $picture = $request->file('picture');
                $pictureName = time() . '.' . $picture->getClientOriginalExtension();
                $picture->move(public_path('online_transactions'), $pictureName);
            }

            DB::beginTransaction();
            $user_id = auth()->user()->id;

            $transaction = new Transaction();
            $transaction->user_id = $user_id;
            $transaction->shipping_fee = 50;
            $transaction->transaction_status = 1;
            $transaction->proof_of_payment = $pictureName ?? null;
            $transaction->comment = $request->comment ?? "";
            $transaction->payment_method = $request->paymentMethod;
            $transaction->pending_at = now();
            $transaction->save();

            foreach ($request->orders as $orderData) {
                $order = new Order();
                $order->transaction_id = $transaction->id;
                $order->product_id = $orderData['id'];
                $order->quantity = $orderData['quantity'];

                $order->save();
            }

            DB::commit();
            $this->updateUI();
            return response()->json(['message' => 'Transaction saved successfully']);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Unable to finish the transaction: ' . $e->getMessage()], 500);
        }
    }

    public function fetchTransaction(Request $request)
    {
        try {
            $user = Auth::user();
            // if ($user) {
            //     if ($request->transaction_user == "admin") {
            //         if (!$user->role) {
            //             throw new \Exception('Invalid status transition');
            //         }
            //     } else {
            //         if (!$this->isUserEmailVerified()) {
            //             throw new \Exception('Email not verified');
            //         }
            //     }
            // }
            DB::beginTransaction();
            $isFinished = $request->transaction_type === 'finished';
            $isAdmin = $request->transaction_user != 'user';

            $model = $isFinished ? FinishedTransaction::class : Transaction::class;

            $transactions = $isAdmin ? $model::with('user', 'orders.item', 'transactionStatus')->get() : $model::with('user', 'orders.item', 'transactionStatus')
                ->where('user_id', $user->id)
                ->get();

            $transactions->transform(function ($transaction) {
                $transaction->created_at_formatted = Carbon::parse($transaction->created_at)->format('F j, Y - h:i A');

                $dateFields = [
                    'pending_at',
                    'processing_at',
                    'delivering_at',
                    'completed_at',
                    'to_return_requested_at',
                    'to_return_accepted_at',
                    'to_return_process_at',
                    'to_return_rejected_at',
                    'return_completed_at',

                    'rejected_at',
                    'canceled_at'
                ];

                foreach ($dateFields as $field) {
                    if ($transaction->$field !== null) {
                        $transaction->$field = Carbon::parse($transaction->$field)->format('F j, Y - h:i A');
                    }
                }

                $transaction->user_name = Crypt::decryptString($transaction->user->name);
                $transaction->user_phone_number = Crypt::decryptString($transaction->user->phone_number);
                $transaction->total_quantity = $transaction->orders->sum('quantity');

                $overallTotal = 0;
                foreach ($transaction->orders as $order) {
                    $order->item_sub_total = $order->quantity * $order->item->price;
                    $overallTotal += $order->item_sub_total; // Change to use the correct property
                }

                $transaction->overall_total = $overallTotal; // Assign overall_total to the transaction, not orders
                return $transaction;


            });

            DB::commit();

            return response()->json(['transactions' => $transactions]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json(['error' => 'Unable to finish the transaction: ' . $e->getMessage()], 500);
        }

    }
    public function fetchSales(Request $request)
    {
        $startingDate = $request->input('starting_date');
        $endingDate = $request->input('ending_date');
        $query = FinishedTransaction::with('user', 'orders.item', 'transactionStatus')
            ->where('transaction_status', 4);


        if ($startingDate) {
            $query->whereDate('created_at', '>=', $startingDate);
        }

        if ($endingDate) {
            $query->whereDate('created_at', '<=', $endingDate);
        }

        if (!$startingDate && !$endingDate) {
            $query->orWhere(function ($query) {
                $query->whereNull('created_at');
            });
        }

        $transactions = $query->get(); // 10 items per page
        $transactions->transform(function ($transaction) {
            $transaction->created_at_formatted = Carbon::parse($transaction->created_at)->format('F j, Y - h:i A');

            $dateFields = [
                'pending_at',
                'processing_at',
                'delivering_at',
                'completed_at',
                'to_return_requested_at',
                'to_return_accepted_at',
                'to_return_process_at',
                'to_return_rejected_at',
                'return_completed_at',

                'rejected_at',
                'canceled_at'
            ];

            foreach ($dateFields as $field) {
                if ($transaction->$field !== null) {
                    $transaction->$field = Carbon::parse($transaction->$field)->format('F j, Y - h:i A');
                }
            }


            $transaction->user_name = Crypt::decryptString($transaction->user->name);
            $transaction->user_phone_number = Crypt::decryptString($transaction->user->phone_number);
            $transaction->total_quantity = $transaction->orders->sum('quantity');

            $overallTotal = 0;
            foreach ($transaction->orders as $order) {
                $order->item_sub_total = $order->quantity * $order->item->price;
                $overallTotal += $order->item_sub_total; // Change to use the correct property
            }

            $transaction->overall_total = $overallTotal; // Assign overall_total to the transaction, not orders
            return $transaction;


        });

        return $transactions;
    }

    public function updateTransactStatus(Request $request)
    {

        try {
            DB::beginTransaction();

            $toReturnComment = $request->to_return_comment;

            if ($toReturnComment) {
                Transaction::where('id', $request->id)->update(['to_return_comment' => $toReturnComment]);
            }

            $this->updateStatus($request->id, $request->transaction_status);
            $this->updateUI();

            DB::commit();
            return response()->json(['message' => 'Transaction status updated successfully']);


        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => 'Unable to finish the updating transaction: ' . $e->getMessage()], 500);
        }


    }
    public function finishTransaction(Request $request)
    {
        try {
            DB::beginTransaction();

            $transactionId = $request->id;
            $newTransactionId = $request->new_transaction_id;
            $rejectComment = $request->reject_comment;


            $orders = Order::where('transaction_id', $transactionId)->get();

            $transaction = Transaction::findOrFail($transactionId);

            if (
                // Pending
                $transaction->transaction_status != 1 &&
                // Completed
                $transaction->transaction_status != 4 &&
                // To Return Requested (can be rejected by the vendor)
                $transaction->transaction_status != 5 &&
                // Return Completed
                $transaction->transaction_status != 8 &&
                // To Return Rejected
                $transaction->transaction_status != 9 &&
                // Rejected
                $transaction->transaction_status != 10 &&
                // Canceled
                $transaction->transaction_status != 11
            ) {
                throw new \Exception('Invalid status transition');
            }

            $finishedTransaction = new FinishedTransaction([
                'id' => $transaction->id,
                'user_id' => $transaction->user_id,
                'shipping_fee' => $transaction->shipping_fee,
                'transaction_status' => $newTransactionId ?? $transaction->transaction_status,
                'payment_method' => $transaction->payment_method,
                'comment' => $transaction->comment,
                'canceled_comment' => $rejectComment ?? null,
                'to_return_comment' => $transaction->to_return_comment,
                'proof_of_payment' => $transaction->proof_of_payment,

                'created_at' => $transaction->created_at,
                'updated_at' => $transaction->updated_at,
                'pending_at' => $transaction->pending_at,
                'processing_at' => $transaction->processing_at,
                'delivering_at' => $transaction->delivering_at,
                'completed_at' => $transaction->completed_at,
                'to_return_requested_at' => $transaction->to_return_requested_at,
                'to_return_accepted_at' => $transaction->to_return_accepted_at,
                'to_return_process_at' => $transaction->to_return_process_at,
                'return_completed_at' => $transaction->return_completed_at,

                'to_return_rejected_at' => $newTransactionId == 9 ? now() : null,
                'rejected_at' => $newTransactionId == 10 ? now() : null,
                'canceled_at' => $newTransactionId == 11 ? now() : null
            ]);

            // Save the finished transaction
            $finishedTransaction->save();

            // Iterate through associated orders and create finished orders
            foreach ($orders as $order) {
                $finishedOrder = new FinishedOrder([
                    'finished_transaction_id' => $finishedTransaction->id,
                    'product_id' => $order->product_id,
                    'quantity' => $order->quantity,
                ]);
                $finishedOrder->save();
            }

            $transaction->delete();
            DB::commit();
            $this->updateUI();
            return response()->json(['message' => 'Transaction marked as finished']);
        } catch (\Exception $e) {
            // Roll back the transaction in case of an error
            DB::rollBack();
            return response()->json(['error' => 'Unable to finish the transaction: ' . $e->getMessage()], 500);
        }
    }
    private function updateStatus($id, $newId)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->transaction_status = $newId;
        
        $transaction->save();

        $this->updateTimeStamp($id, $newId);
    }
    private function updateTimeStamp($id, $transactionStatusId)
    {
        // Define separate arrays for transaction status IDs to their corresponding column names
        $transactionColumnMapping = [
            1 => 'pending_at',
            2 => 'processing_at',
            3 => 'delivering_at',
            4 => 'completed_at',
            5 => 'to_return_requested_at',
            6 => 'to_return_accepted_at',
            7 => 'to_return_process_at',
            8 => 'return_completed_at',
        ];

        $finishedTransactionColumnMapping = [
            9 => 'to_return_rejected_at',
            10 => 'rejected_at',
        ];

        if (array_key_exists($transactionStatusId, $transactionColumnMapping)) {
            $columnName = $transactionColumnMapping[$transactionStatusId];
            $tableName = 'transactions'; // Set the table name
        } elseif (array_key_exists($transactionStatusId, $finishedTransactionColumnMapping)) {
            $columnName = $finishedTransactionColumnMapping[$transactionStatusId];
            $tableName = 'finished_transactions'; // Set the table name
        } else {
            return;
        }

        // Update the timestamp in the appropriate table and column
        DB::table($tableName)->where('id', $id)->update([$columnName => now()]);
    }

    private function isUserEmailVerified()
    {
        return auth()->user()->verified_at ? true : false;
    }

    private function updateUI()
    {
        $user = auth()->user();
        if ($user->role) {
            event(new MessageNotification($user->id));
        } else {
            event(new MessageNotification('Update admin'));
        }
    }

}