<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [
            'id' => (string) $this->id,
            'attributes' => [
                'added_by' => $this->added_by,
                'updated_by' => $this->updated_by,
                'name' => $this->name,
                'description' => $this->description,
                'price' => $this->price,
                'img_url' => $this->img_url,
                'available' => $this->available,
                // 'quantity' => $this->quantity,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at
            ],
        ];
        return $data;
    }
}

