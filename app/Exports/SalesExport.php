<?php

namespace App\Exports;

use App\Models\FinishedTransaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\Exportable;

class SalesExport implements FromCollection, WithHeadings, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function styles(Worksheet $sheet)
    {
        // Apply background color to a specific range of cells (e.g., A1 to B10)
        $sheet->getStyle('A1:H1')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('FEBD59');
        $sheet->getStyle('A1:H1')->getFont()->setBold(true);

        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $range = "A1:{$highestColumn}{$highestRow}";
        $sheet->getStyle($range)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
        $sheet->getStyle($range)->getAlignment()->setWrapText(true);

        // Apply text alignment (e.g., center) to all cells with values
        $sheet->getStyle($range)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        // Manually adjust column widths based on content
        foreach (range('A', $highestColumn) as $column) {
            $sheet->getColumnDimension($column)->setAutoSize(true);
        }
 }

    public function headings(): array
    {
        // $columns = Schema::getColumnListing((new FinishedTransaction)->getTable());
        // return $columns;
        return ["TRANSACTION ID", "ORDER DATE", "PAYMENT TYPE", "BUYER'S NAME", "ITEM NAME", "ITEM PRICE", "QUANTITY", "TOTAL"];
    }

    use Exportable;

    public function collection(){
        $startingDate = Request::input('starting_date');
        $endingDate = Request::input('ending_date');

        $query = FinishedTransaction::with('user', 'orders.item', 'transactionStatus')
        ->where('transaction_status', 4);


        if ($startingDate) {
            $query->whereDate('created_at', '>=', $startingDate);
        }
    
        if ($endingDate) {
            $query->whereDate('created_at', '<=', $endingDate);
        }

        
        if (!$startingDate && !$endingDate) {
            $query->orWhere(function ($query) {
                $query->whereNull('created_at');
            });
        }
    

        $transactions = $query->get(); // 10 items per page

        // ->get();
    
        $ordersData = [];
        $transactions->transform(function ($transaction) use (&$orders, &$ordersData) {
        $transaction->ordered_at = Carbon::parse($transaction->created_at)->format('F j, Y - h:i A');
        $transaction->user_name = Crypt::decryptString($transaction->user->name);
        $transaction->user_phone_number = Crypt::decryptString($transaction->user->phone_number);
        $transaction->total_quantity = $transaction->orders->sum('quantity');
    
        // Create an array of orders for this transaction
        $transaction->orders->each(function ($order) use (&$ordersData, &$orders, $transaction) {
            $order->item_sub_total = $order->quantity * $order->item->price;
    
            $ordersData[] = [
                'transaction_id' => $transaction->id,
                'ordered_at' => $transaction->ordered_at,
                'payment_type' => strtoupper($transaction->payment_method),
                'user_name' => $transaction->user_name,
                'item_name' => $order->item->name,
                'item_price' => $order->item->price,
                'quantity' => $order->quantity,
                'subtotal' => $order->item_sub_total,
            ];
        });
    
        return $transaction;
    });
  
    return collect($ordersData);
    }
}
