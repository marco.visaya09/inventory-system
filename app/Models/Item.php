<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    // protected $fillable = ['added_by', 'updated_by', 'name', 'description', 'price', 'quantity', 'img_url', 'available'];
    protected $fillable = ['added_by', 'updated_by', 'name', 'description', 'price', 'img_url', 'available', 'category', 'quantity'];
    // protected $dateFormat = 'Y-m-d H:i:sO';

    protected $hidden = ['added_by', 'updated_by', 'created_at', 'updated_at'];
    public function user(){
        return $this->belongsTo(User::class);
    }

   
    
}
