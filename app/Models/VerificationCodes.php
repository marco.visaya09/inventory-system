<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerificationCodes extends Model
{
    use HasFactory;

    protected $table = 'verification_codes';
    public $timestamps = false;

    protected $fillable = ['user_id', 'code', 'time_stamp'];
}
