<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinishedTransaction extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'user_id', 'shipping_fee', 'to_return_rejected_at', 'transaction_status', 'payment_method', 'comment', 'canceled_comment', 'created_at', 'updated_at', 'pending_at', 'processing_at', 'delivering_at', 'completed_at', 'to_return_requested_at', 'to_return_accepted_at', 'to_return_process_at', 'to_return_completed_at', 'rejected_at', 'canceled_at', 'to_return_comment', 'proof_of_payment'];

    public function finishedTransaction()
    {
        return $this->hasMany(FinishedTransaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function orders()
    {
        return $this->hasMany(FinishedOrder::class, 'finished_transaction_id');
    }

    public function transactionStatus()
    {
        return $this->belongsTo(TransactionStatus::class, 'transaction_status');
    }


}
