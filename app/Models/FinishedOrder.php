<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinishedOrder extends Model
{
    use HasFactory;
    protected $fillable = ['finished_transaction_id', 'product_id', 'quantity', ];


    public function item()
    {
        return $this->belongsTo(Item::class, 'product_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


}
